import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

def polarizer(vect_in, phi):
    phi = phi/360.0*2.0*np.pi
    pol_mat = np.array([[np.cos(phi)**2,np.cos(phi)*np.sin(phi)],[np.sin(phi)*np.cos(phi),np.sin(phi)**2]])
#    pol_x=x + pol_sign*y
#    pol_y=y+ pol_sign*x
    vect_out = pol_mat.dot(vect_in) 
    return (vect_out)
#%%
mpl.rcParams['legend.fontsize'] = 10

z = np.linspace(1, 20, 1000)


A=2
x0 = z*0
theta = np.pi*0
x = -A*np.cos(z)+np.cos(2*z+theta)
y0 = z*0
y = A*np.sin(z)+np.sin(2*z+theta)
Ebicol = np.array([x,y])
vec_0 = np.array([x0,y0])

E_p = polarizer(Ebicol,90.0)
E_m = polarizer(Ebicol,-45.0)

Int = E_m[0,:]**2+E_m[1,:]**2

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(Ebicol[0,:], Ebicol[1,:], z, label='parametric curve')
ax.plot(E_p[0,:], E_p[1,:], z, label='y')
ax.plot(E_m[0,:], E_m[1,:], z, label='x')
#ax.plot(Int,y0, z, label='x')
ax.set_xlim(-3,3)
ax.set_ylim(-3,3)

ax.legend()
plt.show()

maxInt1 = abs(min(E_p[1,:]))+ abs(max(E_p[1,:]))
maxInt2 = abs(min(E_m[0,:]))+ abs(max(E_m[0,:]))

#ax2.plot(x, y, z, label='parametric curve')
#ax2.plot(mypol_pos[0], mypol_pos[1], z, label='after polarizer at 45%')
#ax2.plot(mypol_neg[0], mypol_neg[1], z, label='after polarizer at -45%')
#ax2.plot(x0, y, z, label='parametric curve')
#ax2.plot(x, y0, z, label='parametric curve')
#ax2.legend()

#plt.show()
#%% Angle Scan
A=2
x0 = z*0
fig2, ax2 = plt.subplots(1,figsize=(15,15)) 
for n in np.linspace(0,0.25,5):
    theta = (2*np.pi)*n
    x2 = -A*np.cos(z)+np.cos(2*z+theta)
    y0 = z*0
    y2 = A*np.sin(z)+np.sin(2*z+theta)
    Ebicol = np.array([x2,y2])

    angles = np.linspace(-180,180,360)
    Int = []
    for m in angles:
        E_m = polarizer(Ebicol,m)
        E2_m = (E_m)**2
        temp_Int = E2_m[0,:]+E2_m[1,:]
        Int.append(temp_Int.max())

    ax2.plot(angles,Int,label='Shift by '+str(round(theta/(2*np.pi),2))+'*Wavelength')
    ax2.grid(b=True, which='both')
    ax2.set_ylabel('E^2 [arb.u.]')
    ax2.set_xlabel('polarization angle [Deg.]')
    ax2.legend()
    ax2.set_title('Intensity ratio ='+str(A**2))
    ax2.set_ylim(1,10)






#%% Rayleigh Lengths
#
#
#zR0 = (100e-6)**2*np.pi/(3e-6)*1e6
#
#zR1 = (100e-6)**2*np.pi/(3e-6/2)*1e6
